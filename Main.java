/**
 * Created by User on 01.02.2017.
 */
public class Main {
    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака", "гав-гав");
        Dog dog2 = new Dog("Тузик", "тяф-тяф");
        Ket ket = new Ket();
        Ket ket1 = new Ket("Кот", "мяу-мяу");
        Ket ket2 = new Ket("Макс", "мр-мр");
        Cassowary cassowary = new Cassowary();
        Cassowary cassowary1 = new Cassowary("Казуар", "арз-арз");

        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();

        ket.printDisplay();
        ket1.printDisplay();
        ket2.printDisplay();

        cassowary.printDisplay();
        cassowary1.printDisplay();

    }
}
